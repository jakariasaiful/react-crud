import React, { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import axios from 'axios';
import Select from 'react-select';
const Add = ({ getAllEmployee, setIsAdding }) => {
  const [name, setName] = useState('');
  const [sector, setSector] = useState(null);
  const [is_term_accept, setIsTermAccept] = useState(false);
  const [sectorList, setSectorList] = useState([]);

   // set data
  useEffect(() => {
    const getSectors = async () => {
      const res = await axios.get( `http://127.0.0.1:8000/api/get-all-sectors` );
      if(res?.data?.success){
        let list = [];
        res?.data.payload.data.forEach(element => {
          list.push({
            value: element.id,
            label: element.name,
          });
        });
        setSectorList(list)
      }
    }
    getSectors();
  }, []);

  console.log(sectorList);

  const handleAdd = async (e) => {
    e.preventDefault();
    debugger;

    if (!name || !sector || !is_term_accept) {
      return Swal.fire({
        icon: 'error',
        title: 'Error!',
        text: 'All fields are required.',
        showConfirmButton: true,
      });
    }

    const newEmployee = {
      name,
      sector: sector.value
    };

    // api call
  const res = await axios.post('http://127.0.0.1:8000/api' + '/save-employee', newEmployee, {
      headers: {},
  });
  getAllEmployee();
  // const employees = await axios.get( `http://127.0.0.1:8000/api/get-all-employee` );
  // if(employees?.data?.success){
  //   setEmployees(res?.data.payload.data);
  // }
  setIsAdding(false);
  if(res.data.success){
    
    Swal.fire({
      icon: 'success',
      title: 'Added!',
      text: `${name} 's data has been Added.`,
      showConfirmButton: false,
      timer: 1500,
    });


  }else{
    Swal.fire({
      icon: 'error',
      title: 'Error!',
      text: 'Something wrong!',
      showConfirmButton: true,
    });
  }
}

    


  const handleChange = (selectedOption) => {
    setSector(selectedOption);
  };

  return (
    <div className="small-container">
      <form onSubmit={handleAdd}>
        <h1>Add New</h1>
        <label htmlFor="name">Name</label>
        <input
          id="name"
          type="text"
          name="name"
          value={name}
          required
          onChange={e => setName(e.target.value)}
        />
        
        
        <label htmlFor="sector">Sector</label>
        <Select
        // value={sector}
        required
        options={sectorList}
        onChange={handleChange}
      />

      <div style={{ marginTop: '10px' }}>
        <input 
        type="checkbox" 
        name='is_term_accept'
        required
        onChange={e => setIsTermAccept(e.target.value)}
        /> Agree to terms
      </div>
        
        <div style={{ marginTop: '30px' }}>
          <input type="submit" value="Add" />
          <input
            style={{ marginLeft: '12px' }}
            className="muted-button"
            type="button"
            value="Cancel"
            onClick={() => setIsAdding(false)}
          />
        </div>
      </form>
    </div>
  );
};

export default Add;
