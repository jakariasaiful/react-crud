import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';

import Header from './Header';
import Table from './Table';
import Add from './Add';
import Edit from './Edit';
import axios from 'axios';

const Dashboard = ({ setIsAuthenticated }) => {
  const [employees, setEmployees] = useState([]);
  const [selectedEmployee, setSelectedEmployee] = useState(null);
  const [isAdding, setIsAdding] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  useEffect(() => {
    // if (data !== null && Object.keys(data).length !== 0) setEmployees(data);
    getAllEmployee();
  }, []);


  const getAllEmployee = async () => {
    const res = await axios.get( `http://127.0.0.1:8000/api/get-all-employee` );
    if(res?.data?.success){
      setEmployees(res?.data.payload.data)
    }
  }

  const handleEdit = id => {
    const [employee] = employees.filter(employee => employee.id === id);
    setSelectedEmployee(employee);
    setIsEditing(true);
  };

  const deleteEmployee = async (id) => {
    const res = await axios.get( `http://127.0.0.1:8000/api/delete-employee/${id}` );
    return res?.data?.success;
  }

  const handleDelete = id => {
    Swal.fire({
      icon: 'warning',
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
    }).then(result => {
      if (result.value) {
        deleteEmployee(id);
        getAllEmployee();
        Swal.fire({
          icon: 'success',
          title: 'Deleted!',
          text: `Deleted Successfully!`,
          showConfirmButton: false,
          timer: 1500,
        });

      }
    });
  };

  return (
    <div className="container">
      {!isAdding && !isEditing && (
        <>
          <Header
            setIsAdding={setIsAdding}
            setIsAuthenticated={setIsAuthenticated}
          />
          <Table
            employees={employees}
            handleEdit={handleEdit}
            handleDelete={handleDelete}
          />
        </>
      )}
      {isAdding && (
        <Add
          employees={employees}
          getAllEmployee={getAllEmployee}
          setIsAdding={setIsAdding}
        />
      )}
      {isEditing && (
        <Edit
          getAllEmployee={getAllEmployee}
          selectedEmployee={selectedEmployee}
          setIsEditing={setIsEditing}
        />
      )}
    </div>
  );
};

export default Dashboard;
