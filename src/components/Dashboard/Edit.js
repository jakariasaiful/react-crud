import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import Select from 'react-select';
const Edit = ({getAllEmployee, selectedEmployee, setIsEditing }) => {
  debugger;
  const id = selectedEmployee.id;
  
  const [name, setName] = useState(selectedEmployee.name);
  const [sector, setSector] = useState(
    {
      value: selectedEmployee?.sector_info.id,
      label: selectedEmployee?.sector_info.name
    }
  );
  const [sectorList, setSectorList] = useState([]);

   // set data
   useEffect(() => {
    const getSectors = async () => {
      const res = await axios.get( `http://127.0.0.1:8000/api/get-all-sectors` );
      if(res?.data?.success){
        let list = [];
        res?.data.payload.data.forEach(element => {
          list.push({
            value: element.id,
            label: element.name,
          });
        });
        setSectorList(list)
      }
    }
    getSectors();
  }, []);

  const handleUpdate  = async (e) => {
    e.preventDefault();

    if (!name || !sector) {
      return Swal.fire({
        icon: 'error',
        title: 'Error!',
        text: 'All fields are required.',
        showConfirmButton: true,
      });
    }

    const employee = {
      name,
      id,
      sector_id: sector.value
    };

    await axios.post('http://127.0.0.1:8000/api' + '/update-employee', employee, {
      headers: {},
    });


    // const res = await axios.get( `http://127.0.0.1:8000/api/get-all-employee` );
    // if(res?.data?.success){
    //   setEmployees(res?.data.payload.data)
    // }

    getAllEmployee();
    setIsEditing(false);

    Swal.fire({
      icon: 'success',
      title: 'Updated!',
      text: `${employee.name} 's data has been updated.`,
      showConfirmButton: false,
      timer: 1500,
    });
  };

  const handleChange = (selectedOption) => {
    debugger;
    setSector(selectedOption);
  };

  return (
    <div className="small-container">
      <form onSubmit={handleUpdate}>
      <h1>Edit</h1>
        <label htmlFor="name">Name</label>
        <input
          id="name"
          type="text"
          name="name"
          value={name}
          required
          onChange={e => setName(e.target.value)}
        />
        
        
        <label htmlFor="sector">Sector</label>
        <Select
        value={sector}
        required
        options={sectorList}
        onChange={handleChange}
      />
        <div style={{ marginTop: '30px' }}>
          <input type="submit" value="Update" />
          <input
            style={{ marginLeft: '12px' }}
            className="muted-button"
            type="button"
            value="Cancel"
            onClick={() => setIsEditing(false)}
          />
        </div>
      </form>
    </div>
  );
};

export default Edit;
